import turtle
import random

#рамка для аквариаума
window = turtle.Screen()
border = turtle.Turtle()
border.speed(0)
border.hideturtle()
border.pensize(3)
border.color('steel blue', 'light blue')
border.begin_fill()
border.up()
border.goto(300, 300)
border.down()
border.goto(300, -300)
border.goto(-300, -300)
border.goto(-300, 300)
border.goto(300, 300)
border.end_fill()

#окружение

#star
star = turtle.Turtle()
def draw_star():
    lenght = random.randrange(10, 60)
    newx = random.randrange(-240, 240)
    newy = random.randrange(-260, 260)

    star.penup()
    star.hideturtle()
    star.speed(0)
    star.goto(newx, newy)
    star.pendown()
    star.color('lightcoral', 'lightcoral')
    star.begin_fill()

    for i in range(1, 6):
        star.forward(lenght)
        star.right(144)
    star.end_fill()
for i in range(10):
    star.speed(0)
    draw_star()

star2 = turtle.Turtle()
def draw_star2():
    lenght2 = random.randrange(10, 60)
    newx2 = random.randrange(-240, 240)
    newy2 = random.randrange(-260, 260)

    star2.penup()
    star2.hideturtle()
    star2.speed(0)
    star2.goto(newx2, newy2)
    star2.pendown()
    star2.color('pink', 'pink')
    star2.begin_fill()

    for i in range(1, 6):
        star2.forward(lenght2)
        star2.right(144)
    star2.end_fill()
for i in range(5):
    star2.speed(0)
    draw_star2()

screen = turtle.Screen()
screen.tracer(0)

don = turtle.Turtle()
don.speed(0)
don.width(3)
don.hideturtle()
def draw_square():
    don.color('steel blue', 'SkyBlue2')
    don.begin_fill()
    don.circle(10)
    don.end_fill()
don.penup()
don.left(100)
don.pendown()


don2 = turtle.Turtle()
don2.goto(50, -150)
don2.speed(0)
don2.width(3)
don2.hideturtle()
def draw_cir():
    don2.color('steel blue', 'SkyBlue2')
    don2.begin_fill()
    don2.circle(30)
    don2.end_fill()

don2.penup()
don2.left(100)
don2.pendown()

don3 = turtle.Turtle()
don3.goto(-100, -200)
don3.speed(0)
don3.width(3)
don3.hideturtle()
def draw_cir2():
    don3.color('steel blue', 'SkyBlue2')
    don3.begin_fill()
    don3.circle(25)
    don3.end_fill()

don3.penup()
don3.left(100)
don3.pendown()

don4 = turtle.Turtle()
don4.goto(-80, -240)
don4.speed(0)
don4.width(3)
don4.hideturtle()
def draw_cir3():
    don4.color('steel blue', 'SkyBlue2')
    don4.begin_fill()
    don4.circle(5)
    don4.end_fill()

don4.penup()
don4.left(100)
don4.pendown()

don5 = turtle.Turtle()
don5.goto(-80, -150)
don5.speed(0)
don5.width(3)
don5.hideturtle()
def draw_cir4():
    don5.color('steel blue', 'SkyBlue2')
    don5.begin_fill()
    don5.circle(15)
    don5.end_fill()

don5.penup()
don5.left(100)
don5.pendown()

#черепашка
tur = turtle.Turtle()
tur.shape('turtle')
tur.up()
dx = 0.2
dy = 0.5

#передвижение черепашки
while True:
    x, y = tur.position()
    if x + dx >= 290 or x + dx <= -290:
        dx = -dx
    if y + dy >= 290 or y + dy <= -290:
        dy = -dy
    tur.goto(x + dx, y + dy)

    don.clear()
    draw_square()
    screen.update()        
    don.forward(0.12)

    don2.clear()
    draw_cir()
    screen.update()        
    don2.forward(0.11)

    don3.clear()
    draw_cir2()
    screen.update()         
    don3.forward(0.09)

    don4.clear()
    draw_cir3()
    screen.update()         
    don4.forward(0.10)

    don5.clear()
    draw_cir4()
    screen.update()
    don5.forward(0.05)

window.mainloop()
