import pygame
from pygame.sprite import Sprite

pygame.init()

from pygame.locals import (RLEACCEL)

SCREEN_WIDTH = 300
SCREEN_HEIGHT = 150

screen = pygame.display.set_mode((SCREEN_WIDTH, SCREEN_HEIGHT))
pygame.display.set_caption("Mario")


class Player(Sprite):
    def __init__(self, x, y):
        super().__init__()
        self.images_front_and_back = [pygame.image.load("0.png").convert()]
        self.images_right = [pygame.image.load("r1.png").convert(), pygame.image.load("r2.png").convert(),
                             pygame.image.load("r3.png").convert(), pygame.image.load("r4.png").convert(),
                             pygame.image.load("r5.png").convert()]
        self.images_left = [pygame.image.load("l1.png").convert(), pygame.image.load("l2.png").convert(),
                            pygame.image.load("l3.png").convert(), pygame.image.load("l4.png").convert(),
                            pygame.image.load("l5.png").convert()]
        self.index = 0
        self.image = self.images_right[self.index]
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y
        self.velocity_x = 0
        self.velocity_y = 0

    def update(self):
        keys = pygame.key.get_pressed()
        if keys[pygame.K_s]:
            if self.index >= len(self.images_front_and_back):
                self.index = 0
            self.image = self.images_front_and_back[self.index]
        if keys[pygame.K_w]:
            if self.index >= len(self.images_front_and_back):
                self.index = 0
            self.image = self.images_front_and_back[self.index]
        if keys[pygame.K_a]:
            self.velocity_x = -4
            self.index += 1
            if self.index >= len(self.images_left):
                self.index = 0
            self.image = self.images_left[self.index]
        elif keys[pygame.K_d]:
            self.velocity_x = 4
            self.index += 1
            if self.index >= len(self.images_right):
                self.index = 0
            self.image = self.images_right[self.index]
        else:
            self.velocity_x = 0

        self.rect.x += self.velocity_x

        # ограничения справа из-за конца карты
        if self.rect.x < 0:
            self.rect.x = 0
        elif self.rect.x > SCREEN_WIDTH - 34:
            self.rect.x = SCREEN_WIDTH - 34

        # ограничение слева из-за гриба
        if self.rect.x < 0:
            self.rect.x = 0
        elif self.rect.x < SCREEN_WIDTH - 270:
            self.rect.x = SCREEN_WIDTH - 270


class Mush1Sprite(Sprite):
    def __init__(self, x, y, width, height):
        super().__init__()
        self.image = pygame.image.load("grib.png").convert()
        self.image.set_colorkey((0, 0, 0), RLEACCEL)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y


class Block1Sprite(Sprite):
    def __init__(self, x, y, width, height):
        super().__init__()
        self.image = pygame.image.load("block.png").convert()
        self.image.set_colorkey((0, 0, 0), RLEACCEL)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y


class GroundSprite(Sprite):
    def __init__(self, x, y, width, height):
        super().__init__()
        self.image = pygame.image.load("ground.png").convert()
        self.image.set_colorkey((0, 0, 0), RLEACCEL)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y


class BlockkirSprite(Sprite):
    def __init__(self, x, y, width, height):
        super().__init__()
        self.image = pygame.image.load("block2.png").convert()
        self.image.set_colorkey((0, 0, 0), RLEACCEL)
        self.rect = self.image.get_rect()
        self.rect.x = x
        self.rect.y = y


all_sprites = pygame.sprite.Group()
player = Player(SCREEN_WIDTH / 2, SCREEN_HEIGHT / 2)
eshe = Mush1Sprite(SCREEN_WIDTH/2 - 140, SCREEN_HEIGHT/2 - 0, 50, 50)
korobka = Block1Sprite(SCREEN_WIDTH/2 - -50, SCREEN_HEIGHT/2 - 60, 50, 50)
zemlya = GroundSprite(SCREEN_WIDTH/2 - 250, SCREEN_HEIGHT/2 - -33, 50, 50)
kirpich1 = BlockkirSprite(SCREEN_WIDTH/2 - -70, SCREEN_HEIGHT/2 - 60, 50, 50)
kirpich2 = BlockkirSprite(SCREEN_WIDTH/2 - -30, SCREEN_HEIGHT/2 - 60, 50, 50)
all_sprites.add(kirpich2)
all_sprites.add(kirpich1)
all_sprites.add(zemlya)
all_sprites.add(eshe)
all_sprites.add(korobka)
all_sprites.add(player)


clock = pygame.time.Clock()

running = True
while running:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False

    all_sprites.update()

    screen.fill((90, 152, 196))
    all_sprites.draw(screen)

    pygame.display.flip()
    clock.tick(12)

pygame.quit()
