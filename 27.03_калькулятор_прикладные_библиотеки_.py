from tkinter import *
from tkinter import ttk
import math

# root = Tk()
# frm = ttk.Frame(root, padding=10)
# frm.grid()
# #ttk.Label(frm, text="Hello my friend! ✨⭐✨").grid(column=0, row=0)
# ttk.Label(frm, text="Hello my friend! ✨⭐✨").pack()
# #ttk.Button(frm, text="Quit", command=root.destroy).grid(column=1, row=0)
# ttk.Button(frm, text="Quit", command=root.destroy).pack()
#
# root.mainloop()

#pack - делает части интерфейса в отдельном окне
def calculation():
    number = first.get()
    number2 = second.get()
    number_itog = number2 + number
    itog.set(str(number_itog))

root = Tk()
root.title("Калькулятор суммы")
root.geometry("430x130")

first = IntVar()
second = IntVar()
itog = IntVar()
itog.set("")
first.set("")
second.set("")

frm = ttk.Frame(root, padding=10)
frm.grid()
ttk.Label(root, text="слагаемое №1").place(x=25, y=10)
ttk.Label(root, text="слагаемое №2").place(x=170, y=10)
ttk.Label(root, text="Ответ").place(x=340, y=10)
ttk.Entry(root, textvariable=first).place(x=5, y=40)
ttk.Label(root, text="+").place(x=135, y=40)
ttk.Entry(root, textvariable=second).place(x=150, y=40)
ttk.Label(root, text="=").place(x=280, y=40)
ttk.Entry(root, textvariable=itog).place(x=295, y=40)

jmak = Button(text="Нажми для результата", command=calculation).place(x=145, y=80)
#ttk.Button(frm, text="Посчитать", command=root.destroy).grid(column=5, row=50)


root.mainloop()
