import tkinter as tk
from tkinter import *
import re
from tkinter import filedialog
from tkinter import scrolledtext
import numpy

window = tk.Tk()
text_path = filedialog.askopenfilename(title='Выберите .txt документ')
window.destroy()
with open(text_path, 'r', encoding='utf-8') as file:
    Text = file.read()
    Text = Text.replace("\t", "")
Text = Text.split(sep='\n')
pattern = r'^\d{1}\.'  # строка начинается с цифры, потом точка
Text_q = [i for i in Text if len(re.findall(pattern, i)) != 0]
pattern = r'^\d{1}\)'  # строка начинается с цифры, потом скобка
Text_a = [i for i in Text if len(re.findall(pattern, i)) != 0]
pattern = r'\+'  # ищем строки с меткой +
Text_a_last = []
flags = numpy.zeros(100)  # создание массив из нулей ([ 0., 0., 0., 0., 0, ...])
for i, st in enumerate(Text_a):
    if len(re.findall(pattern, st)) != 0:  # если в строке найдены метки
        new_i = re.sub(r'\+', '', st)  # удаляем метки
        Text_a_last.append(new_i)
        flags[i] = 1
    else:
        Text_a_last.append(st)
Text_a = Text_a_last
Text_q_dict = {}
for i, q in enumerate(Text_q):
    Text_q_dict[i] = q
np1 = numpy.arange(len(Text_q))
order_list = np1.tolist()


class EverythingInFrame:
    def __init__(self, master):
        self.qc = 0
        self.true_points = 0
        self.quest = scrolledtext.ScrolledText(window, width=71, height=5)
        index = order_list[self.qc]  # индекс вопроса определяем по order_list
        self.quest.insert(tk.INSERT, Text_q[index])
        self.ans = scrolledtext.ScrolledText(window, width=44, height=15)
        self.ans.insert(tk.INSERT, f''' 
        {Text_a[4 * index + 0]}
        
        {Text_a[5 * index + 1]}
        
        {Text_a[5 * index + 2]}
        
        {Text_a[5 * index + 3]}
        
        {Text_a[5 * index + 4]}     ''')
        self.check1 = tk.IntVar()  # в данную переменную записывается состояние box1 (1 или 0)
        self.box1 = Radiobutton(text='1', variable=self.check1, value="1")
        # self.check2 = tk.IntVar()
        self.box2 = Radiobutton(text='2', variable=self.check1, value="2")
        # self.check3 = tk.IntVar()
        self.box3 = Radiobutton(text='3', variable=self.check1, value="3")
        # self.check4 = tk.IntVar()
        self.box4 = Radiobutton(text='4', variable=self.check1, value="4")
        # self.check5 = tk.IntVar()
        self.box5 = Radiobutton(text='5', variable=self.check1, value="5")
        self.box1.pack()
        self.box2.pack()
        self.box3.pack()
        self.box4.pack()
        self.box5.pack()
        self.ButGiveAns = Button(text='Ответить', font=('Arial Bold', 12))  # кнопка перехода в состояние "ПРОВЕРКА"
        self.ButGiveAns['command'] = self.show_res
        self.ButNext = Button(text='Следующий вопрос',
                              font=('Arial Bold', 12))  # кнопка перехода в состояние "СМЕНА ВОПРОСА"
        self.ButNext['command'] = self.next_q
        self.quest.place(x=25, y=35)
        self.ans.place(x=75, y=155)
        self.box1.place(x=25, y=155)
        self.box2.place(x=25, y=195)
        self.box3.place(x=25, y=235)
        self.box4.place(x=25, y=275)
        self.box5.place(x=25, y=315)
        self.ButGiveAns.place(x=75, y=420)
        self.ButNext.place(x=175, y=420)

    def show_res(self):
        index = order_list[self.qc]
        targets = flags[5 * index: 5 * index + 5]
        answers = numpy.zeros(5)
        answers[0] = self.check1.get()  # записываем состояние box1 (0 или 1) в нулевой бит вектора answers чтобы посчитать баллы
        answers[1] = self.check1.get()
        answers[2] = self.check1.get()
        answers[3] = self.check1.get()
        answers[4] = self.check1.get()
        for i, box in enumerate([self.box1, self.box2, self.box3, self.box4, self.box5]):
            if targets[i] == 1:
                box['bg'] = 'yellow green'
            else:
                box['bg'] = 'firebrick1'
        if (targets == answers).sum() == 5:
            self.true_points += 1  # всё верно - плюс балл
        else:
            self.true_points += 0

    def next_q(self):
        self.qc += 1
        if self.qc >= len(Text_q):
            window = tk.Toplevel()
            window.title('Итог теста')
            window.resizable(width=False, height=False)
            window.geometry('310x400')
            window['bg'] = 'burlywood3'
            self.FinalScore = tk.Label(window, text=f'Всего правильных ответов: {self.true_points}', font=(15),
                                       fg='black', bg='burlywood3')
            self.FinalScore.place(x=5, y=5)
            self.img = PhotoImage(file='Klepa.png')
            Label(window, image=self.img).place(x=3, y=35)
            Label(window, text="(Спасибо моему кошачьему ассистенту за", bg='burlywood3').place(x=5, y=345)
            Label(window, text="проделанную работу лапками)", bg='burlywood3').place(x=5, y=365)
        else:
            index = order_list[self.qc]
            for i, box in enumerate([self.box1, self.box2, self.box3, self.box4, self.box5]):
                box['bg'] = 'white'
                box.deselect()  # убираем галочки
            self.quest.delete('1.0', 'end')  # очищаем всё поле с индекса "1" до последнего "end"
            self.quest.insert(tk.INSERT, Text_q[index])  # выводим следующий вопрос
            self.ans.delete('1.0', 'end')
            self.ans.insert(tk.INSERT, f'''
            {Text_a[5 * index + 0]}
            
            {Text_a[5 * index + 1]}
            
            {Text_a[5 * index + 2]}
            
            {Text_a[5 * index + 3]}
            
            {Text_a[5 * index + 4]}     ''')


window = tk.Tk()
window.title('Тест')
window.resizable(width=False, height=False)
window.geometry('640x480')
window['bg'] = 'burlywood3'
Label(window, text="Вопрос", font=('Arial Bold', 12), bg='burlywood3').place(x=25, y=10)
Label(window, text="Ответы", font=('Arial Bold', 12), bg='burlywood3').place(x=75, y=130)
window.img = PhotoImage(file='cat.png')
Label(window, image=window.img, bg='burlywood3').place(x=470, y=300)
window.img2 = PhotoImage(file='cat2.png')
Label(window, image=window.img2, bg='burlywood3').place(x=530, y=200)
first_block = EverythingInFrame(window)
window.mainloop()
