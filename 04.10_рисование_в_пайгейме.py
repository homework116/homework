import pygame
import random
pygame.init()

screen_width = 300
screen_height = 300
screen = pygame.display.set_mode((screen_width, screen_height))
screen.fill((255, 255, 255))

# цвет квадрата
color = (255, 0, 0)
# координаты квадрата
x = 50
y = 50
# квадрат
width = 10
height = 10

colors = [(255, 0, 0), (255, 106, 0), (255, 247, 0), (63, 204, 68), (27, 211, 224), (27, 211, 224), (180, 21, 237)]
color_index = 0
square_color = colors[color_index]

while True:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            pygame.quit()
            quit()
        elif event.type == pygame.KEYDOWN:
            if event.key == pygame.K_UP:
                y -= 10
            elif event.key == pygame.K_DOWN:
                y += 10
            elif event.key == pygame.K_LEFT:
                x -= 10
            elif event.key == pygame.K_RIGHT:
                x += 10
            if event.key == pygame.K_c:
                square_color = colors[random.randint(0, len(colors)-1)]
            if event.key == pygame.K_x:
                square_color = (255, 255, 255)

    pygame.draw.rect(screen, square_color, [x, y, width, height])

    pygame.display.update()