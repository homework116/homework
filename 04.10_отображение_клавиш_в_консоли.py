import pygame
from pygame.locals import *

pygame.init()

display = pygame.display.set_mode((300, 300))
display.fill((255, 255, 255))

game_stop = False
while not game_stop:
    for event in pygame.event.get():
        if event.type == QUIT:
            print("QUIT")
            # pygame.quit()
            game_stop = True
        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_RETURN:
                print("ENTER")
            if event.key == pygame.K_SPACE:
                print("SPACE")
            if event.key == pygame.K_w:
                print("W")
            if event.key == pygame.K_a:
                print("A")
            if event.key == pygame.K_s:
                print("S")
            if event.key == pygame.K_d:
                print("D")
            if event.key == pygame.K_ESCAPE:
                print("Esc")
            if event.key == pygame.K_UP:
                print("Up Key")
            if event.key == pygame.K_DOWN:
                print("Down Key")
            if event.key == pygame.K_LEFT:
                print("Left Key")
            if event.key == pygame.K_RIGHT:
                print("Right Key")
    pygame.display.flip()
